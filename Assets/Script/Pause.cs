﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Pause : MonoBehaviour
{
     public GameObject PauseMenu ;
     bool isPaused ;
     [SerializeField] Button _resumeButton;
     [SerializeField] Button _pauseButton;
     [SerializeField] Button _mainButton;
     [SerializeField] Button _exitButton;
    // Start is called before the first frame update
    void Start()
    {
        _pauseButton.onClick.AddListener (
            delegate { PauseButtonClick (_pauseButton); });
        _mainButton.onClick.AddListener (
            delegate { MainButtonClick (_mainButton); });
        _exitButton.onClick.AddListener (
            delegate { ExitButtonClick (_exitButton); });
        _resumeButton.onClick.AddListener (
            delegate { ResumeButtonClick (_resumeButton); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PauseButtonClick (Button button) {
       if(PauseMenu != null)
        {
          PauseMenu.SetActive(true);
        }
        if(isPaused == false)
        {
            Time.timeScale = 0;
            isPaused = true ; 
        }
    }
    public void MainButtonClick (Button button) {
         SceneManager.LoadScene ("SceneMainMenu");
    }
    public void ResumeButtonClick (Button button) {
        if(PauseMenu != null)
        {
          PauseMenu.SetActive(false);
        }
        if(isPaused == true)
        {
            Time.timeScale += 1;
            isPaused = false ; 
        }
           
    }
    public void ExitButtonClick (Button button) {
       Application.Quit ();
    }

}
