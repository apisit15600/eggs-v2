﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeBar : MonoBehaviour
{
    Image Bar ;
    public float maxTime = 60.0f ;
    public float timeLeft ;
    public GameObject Gameover ;
    // Start is called before the first frame update
    void Start()
    {
        timeLeft = maxTime ;
        Bar = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            Bar.fillAmount = timeLeft / maxTime ;
        }else
        {
             Gameover.SetActive(true);
             Time.timeScale = 0 ;
        }
    }
}
