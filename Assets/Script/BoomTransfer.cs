﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoomTransfer : MonoBehaviour
{

    public float speed = 0.1f;
    public float maxPos = 2.0f;
    public float minPos = -2.0f;
    
    bool isRight = true;
    
    Vector3 _position;

    // Start is called before the first frame update
    void Start()
    {


        _position = transform.position;

       
        

    }

    // Update is called once per frame
    void Update()
    {

        

        ChangePosition();



    }

    void ChangePosition()
    {
        if (isRight) {
            transform.Translate(new Vector3(0.1f, 0, 0) * speed);

            if (_position.x >= maxPos)
            {
                isRight = false;
            }

        }
        else
        {
            transform.Translate(new Vector3(-0.1f, 0, 0) * speed);
            if (_position.x <= minPos)
            {
                isRight = true;
            }


        }



    }
}
