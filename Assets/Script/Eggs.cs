﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eggs : MonoBehaviour {
    public int eggValue = 1;
    public void OnCollisionEnter2D (Collision2D other) {
        if (other.gameObject.CompareTag ("Player")) {
            GameObjectsSpawnManager.instance.ChangeScore(eggValue);
        }
    }

}