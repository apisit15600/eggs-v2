﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class PlayerCollisionCheck : MonoBehaviour
{
    bool ProtectP;
    bool isPaused;
    public GameObject Gameover;
    public TimeBar newTime;
    public GameObject Shield;
    public AudioSource GetItem ;
    public float TimePlayer = 0 ;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
            if(ProtectP == true)
            {
                 TimePlayer += Time.deltaTime;
            if( TimePlayer > 5)
            {
                Shield.SetActive(false);
                ProtectP = false ;
                TimePlayer = 0 ;
            }
            }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        
        if (other.gameObject.CompareTag ("Item"))
        {
            GetItem.Play();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Obstacle"))
        {
            Destroy(other.gameObject);
            Time.timeScale = 0 ;
            Gameover.SetActive(true);
            
        }
        if (other.gameObject.CompareTag("Armor"))
        {
            Shield.SetActive(true);
            Destroy(other.gameObject);
            ProtectP = true ;
        }
        if (other.gameObject.CompareTag("Heal"))
        {
            newTime.timeLeft += 3; 
            Destroy(other.gameObject);


            //Debug.Log("Times :+ newTime.timeLeft ") ;
        }

    }
}
