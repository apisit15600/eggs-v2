﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro ;

public class Scoremanager : MonoBehaviour
{
    public static Scoremanager instance;
    public TextMeshProUGUI text ;
     public TextMeshProUGUI text2 ;
     public TextMeshProUGUI Level ;
    int score ;
    
    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this ;
        }
    }
    public void ChangeScore(int eggValue)
    {
        score += eggValue;
        text.text = "X"+" " + score.ToString();
         text2.text = "X"+" " + score.ToString();
    }

    // Update is called once per frame
    void Update()
    {

        ChangeLevel();
        
    }
    public void ChangeLevel()
    {
       

        if(score  == 5  ){

        Level.text = "Level : "+ 2;
        }
        

        if(score  == 10  ){

        Level.text = "Level : "+ 3;
        }
        

    }



     
}
