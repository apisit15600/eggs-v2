﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RandomRate : MonoBehaviour
{
    

    [System.Serializable]
    public class DropCurrency
    {
        public string name;

        public GameObject item;

        public int dropRarity;   
       
       
       

    }


    public List <DropCurrency> LootTable = new List<DropCurrency>();
   
    public int dropChance;


    void Start () {
        Invoke ("SpawnItem", 2.0f);
        
    }
    // Update is called once per frame
    void Update () {

    }

    public void CalculateLoot()
    {
        int calc_DropChance = Random.Range(0,101);

        if(calc_DropChance > dropChance)
        {
            Debug.Log("NO LOOT FOR ME");
            return;

        } 
        if(calc_DropChance <= dropChance)
        {
            int itemWeight = 0;
            for (int i = 0; i < LootTable.Count; i++ )
            {
                itemWeight += LootTable [i].dropRarity;


            }
            Debug.Log("ItemWeight = "+itemWeight);

            int randomValue = Random.Range (0,itemWeight);

            for(int j = 0; j< LootTable.Count; j++)
            {
                if(randomValue <= LootTable[j].dropRarity)
                {
                     GameObject go = Instantiate(LootTable[j].item,new Vector3 (Random.Range (-4.5f, 4.5f), 5),Quaternion.identity);
                     Destroy (go,1.5f);
                     
                     float nextSpawnTime = Random.Range (0.2f, 1);
                     Invoke ("SpawnItem", nextSpawnTime);
                }
                randomValue -= LootTable[j].dropRarity;
                Debug.Log("Random Value decresed"+randomValue);


            }



        } 




    }

    
       

}
