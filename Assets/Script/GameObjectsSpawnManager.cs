﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class GameObjectsSpawnManager : MonoBehaviour {
    public static GameObjectsSpawnManager instance;
    public TextMeshProUGUI Eggtext;
    public TextMeshProUGUI Eggtext2;
    public TextMeshProUGUI Level;
    int levelState  ;
    int score;
    float nextSpawnTime ;
    float nextSpawnTimeob = 5;
    float nextSpawnTimeob1 = 7;
    float nextSpawnTimeob2 = 10;
    // float firstSpawnTime_SpawnObstacle ;

    [SerializeField]
    GameObject _itemToBeSpawn;
    [SerializeField]
    GameObject _healToBeSpawn;
    [SerializeField]
    GameObject _ShieldToBeSpawn;
    [SerializeField]
    GameObject _obstaclesToBeSpawn;
    [SerializeField]
    GameObject _obstaclesToBeSpawn2;
    [SerializeField]
    GameObject _obstaclesToBeSpawn3;
    [SerializeField]
    GameObject _obstaclesToBeSpawn4;


    // Use this for initialization
    void Start () {

      

        if (instance == null)
        {
            instance = this;
        }
 
        Invoke("SpawnItem", 2.0f);
        Invoke("SpawnHeal", 15.0f);
        Invoke("SpawnShiel", 23.0f);
        Invoke("SpawnObstacle",10.0f);
        Invoke("SpawnObstacle2", 20.0f);
        Invoke("SpawnObstacle3", 20.0f);
        Invoke("SpawnObstacle4", 30.0f);
    }
    // Update is called once per frame
    void Update () {

        

        ChangeLevel();

        
        
        
    }
    public void ChangeScore(int eggValue)
    {
        for(int i = 1 ; i <50 ;i++)
        {
            if( levelState == i )
            {
              score += eggValue * i;
            }
            
        }

        Eggtext.text = "X" + " " + score.ToString();
        Eggtext2.text = "X" + " " + score.ToString();
        
    }
    public void ChangeLevel()
    {
         
       
         if (score == 0 )
        {
            levelState = 1;
        
        }

        if (score >= 500 )
        {
            levelState = 2;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 4.5f;
            nextSpawnTimeob1 = 6.5f;
            nextSpawnTimeob2 = 9.5f;
        }
        if (score >= 2000)
        {
            levelState = 3;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 4;
            nextSpawnTimeob1 = 6;
            nextSpawnTimeob2 = 9;

        }
        if (score >= 4000)
        {
            levelState = 4;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 3.5f;
            nextSpawnTimeob1 = 5.5f;
            nextSpawnTimeob2 = 8.5f;


        }
        if (score >= 6500)
        {
            levelState = 5;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 3;
            nextSpawnTimeob1 = 5;
            nextSpawnTimeob2 = 8;


        }
        if (score >= 9500)
        {
            levelState = 6;
            Level.text = "Level : " + levelState;

            nextSpawnTimeob = 2.5f;
            nextSpawnTimeob1 = 4.5f;
            nextSpawnTimeob2 = 7.5f;
        }
        if (score >= 12500)
        {
            levelState = 7;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 2;
            nextSpawnTimeob1 = 4;
            nextSpawnTimeob2 = 7;

        }
        if (score >= 16000)
        {
            levelState = 8;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 1.5f;
            nextSpawnTimeob1 = 3.5f;
            nextSpawnTimeob2 = 6.5f;
        }
        if (score >= 20000)
        {
            levelState = 9;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 1;
            nextSpawnTimeob1 = 3;
            nextSpawnTimeob2 = 6;
        }
        if (score >= 24500)
        {
            levelState = 10;
            Level.text = "Level : " + levelState;
            nextSpawnTimeob = 0.5f;
            nextSpawnTimeob1 = 2.5f;
            nextSpawnTimeob2 = 5.5f;
        }


    }
   
    
    void SpawnItem () {
        GameObject go = Instantiate (_itemToBeSpawn,new Vector3 (Random.Range (-2.0f, 2.0f), 3,Random.Range (-2.0f, 2.0f)),Quaternion.identity);

        float nextSpawnTime = Random.Range (0.2f, 1);
        Invoke ("SpawnItem", nextSpawnTime);
    }

    void SpawnObstacle () {
       // int obsIdx = Random.Range (0, _obstaclesToBeSpawn.Length);
        GameObject go = Instantiate (_obstaclesToBeSpawn,new Vector3 (Random.Range (-2.0f, 2.0f), 3,Random.Range (-2.0f, 2.0f)), Quaternion.identity);
   

         
        Invoke ("SpawnObstacle", nextSpawnTimeob);
    }
    void SpawnObstacle2()
    {
        // int obsIdx = Random.Range (0, _obstaclesToBeSpawn.Length);
        GameObject go = Instantiate(_obstaclesToBeSpawn2, new Vector3(Random.Range(-2.0f, 2.0f), 3, Random.Range(-2.0f, 2.0f)), Quaternion.identity);


        
        Invoke("SpawnObstacle2", nextSpawnTimeob1);
    }
    void SpawnObstacle3()
    {
        // int obsIdx = Random.Range (0, _obstaclesToBeSpawn.Length);
        GameObject go = Instantiate(_obstaclesToBeSpawn3, new Vector3(Random.Range(-2.0f, 2.0f), 5, Random.Range(-2.0f, 2.0f)), Quaternion.identity);


        
        Invoke("SpawnObstacle3", nextSpawnTimeob1);
    }
    void SpawnObstacle4()
    {
        // int obsIdx = Random.Range (0, _obstaclesToBeSpawn.Length);
        GameObject go = Instantiate(_obstaclesToBeSpawn4, new Vector3(Random.Range(-2.0f, 2.0f), 5, Random.Range(-2.0f, 2.0f)), Quaternion.identity);


        
        Invoke("SpawnObstacle3", nextSpawnTimeob2);
    }
    void SpawnHeal()
    {
        GameObject go = Instantiate(_healToBeSpawn, new Vector3(Random.Range(-2.0f, 2.0f), 5, Random.Range(-2.0f, 2.0f)), Quaternion.identity);


         nextSpawnTime = 10;
        Invoke("SpawnHeal", nextSpawnTime);
    }
     void SpawnShiel()
    {
        GameObject go = Instantiate(_ShieldToBeSpawn, new Vector3(Random.Range(-2.0f, 2.0f), 5, Random.Range(-2.0f, 2.0f)), Quaternion.identity);


         nextSpawnTime = 15;
        Invoke("SpawnShiel", nextSpawnTime);
    }
}