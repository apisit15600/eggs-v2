﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour
{
    [SerializeField] Button _mainButton;
     [SerializeField] Button _restartButton;
     [SerializeField] Button _exitButton;
     bool isPaused = false ;
    // Start is called before the first frame update
    void Start()
    {
         _mainButton.onClick.AddListener (
            delegate { MainButtonClick (_mainButton); });
         _restartButton.onClick.AddListener (
            delegate { RestartButtonClick (_restartButton); });
        _exitButton.onClick.AddListener (
            delegate { ExitButtonClick (_exitButton); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
      public void MainButtonClick (Button button) {
         SceneManager.LoadScene ("SceneMainMenu");
    }
     public void RestartButtonClick (Button button) {
         SceneManager.LoadScene ("SceneGameplay");
           if(isPaused == false)
        {
            Time.timeScale = 1 ;
            isPaused = true ; 
        }
    }

    public void ExitButtonClick (Button button) {
       Application.Quit ();
    }
}
