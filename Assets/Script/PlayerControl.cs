﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {
    public float movespeed;
    private Vector3 touchPosition;
    private Vector3 direction;
    private Rigidbody2D rb;
    private Vector3 PlayerPosition;

    // Start is called before the first frame update
    void Start () {
        rb = GetComponent<Rigidbody2D> ();

        //This.position = PlayerPosition.position;
    }

    // Update is called once per frame
    void Update () {
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch (0);
            touchPosition = Camera.main.ScreenToWorldPoint (touch.position);
            touchPosition.z = 0;
            direction = (touchPosition - transform.position);
            rb.velocity = new Vector2 (direction.x, 0) * movespeed;
            if (touch.phase == TouchPhase.Ended)
                rb.velocity = Vector2.zero;
        }
        /*if (Input.GetKeyDown (KeyCode.LeftArrow)) {
            PlayerPosition = transform.position;

            rb.velocity = new Vector2 (-1, 0) * movespeed;
        } else {
            rb.velocity = Vector2.zero;

        }

        if (Input.GetKeyDown (KeyCode.RightArrow)) {
            PlayerPosition = transform.position;

            rb.velocity = new Vector2 (1, 0) * movespeed;

        }*/

    }
}