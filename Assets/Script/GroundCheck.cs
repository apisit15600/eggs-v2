﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnCollisionEnter2D (Collision2D other)
    {
        if(other.gameObject.CompareTag ("Obstacle"))
        {
            Destroy(other.gameObject) ;
        }
        if(other.gameObject.CompareTag ("Item"))
        {
            Destroy(other.gameObject) ;
        }
         if (other.gameObject.CompareTag("Armor"))
        {
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Heal"))
        {
            Destroy(other.gameObject);
        }
    }
}
